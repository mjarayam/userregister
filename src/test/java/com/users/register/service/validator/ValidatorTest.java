package com.users.register.service.validator;

import static org.junit.Assert.*;

import constants.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ValidatorTest {

  @InjectMocks
  private Validator validator;

  @Test
  public void shouldValidate() {
    String email = "test@test.com";
    Boolean response = validator.validator(email, Constants.EMAIL_REGEX);

    assertNotNull(response);
    assertTrue(response);
  }

  @Test
  public void shouldNotValidate() {
    String email = "ksljsajl";
    Boolean response = validator.validator(email, Constants.EMAIL_REGEX);

    assertNotNull(response);
    assertFalse(response);
  }


}
