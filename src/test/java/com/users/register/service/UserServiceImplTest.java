package com.users.register.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.users.register.data.UserEntity;
import com.users.register.data.UserPhoneEntity;
import com.users.register.dto.UserPhoneDto;
import com.users.register.dto.UserRequestDto;
import com.users.register.dto.UserResponseDto;
import com.users.register.repository.UsersRepository;
import com.users.register.service.validator.Validator;
import constants.Constants;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;
import javax.xml.bind.ValidationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.HttpServerErrorException;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

  @InjectMocks
  private UserServiceImpl userService;
  @Mock
  private UsersRepository usersRepository;
  @Mock
  private Validator validator;

  private UserRequestDto request;
  private UserEntity userEntity;

  @Before
  public void setUp() throws Exception {
    UserPhoneDto phone = new UserPhoneDto();
    phone.setNumber(22323233l);
    phone.setCityCode(32l);
    phone.setCountryCode(56l);

    UserPhoneEntity phoneEntity = UserPhoneEntity.builder()
        .number(22323233l)
        .cityCode(32l)
        .countryCode(56l)
        .build();
    request = new UserRequestDto();
    request.setName("Carlos Martínez");
    request.setEmail("cmartinez@test.com");
    request.setPassword("Carlos23");
    request.setPhone(phone);

    userEntity = UserEntity.builder()
        .id(3L)
        .name("Carlos Martínez")
        .email("cmartinez@test.com")
        .password("Carlos23")
        .phone(phoneEntity)
        .active(Boolean.TRUE)
        .creationDate(LocalDate.now())
        .lastModification(LocalDate.now())
        .token(UUID.randomUUID().toString())
        .build();
  }

  @Test
  public void shouldSaveNewUser() throws Exception {
    when(validator.validator(anyString(), anyString())).thenReturn(Boolean.TRUE);
    when(usersRepository.findByEmail(anyString())).thenReturn(Optional.empty());
    when(usersRepository.save(any())).thenReturn(userEntity);

    UserResponseDto responseDto = userService.register(request);

    verify(validator, times(2)).validator(any(), any());
    verify(usersRepository).findByEmail(any());
    verify(usersRepository).save(any());
    assertNotNull(responseDto);
    assertNotNull(responseDto.getId());
    assertNotNull(responseDto.getToken());
    assertNotNull(responseDto.getCreated());
    assertNotNull(responseDto.getModified());
    assertNotNull(responseDto.getLastLogin());
    assertTrue(responseDto.isActive());
  }

  @Test(expected = ValidationException.class)
  public void shouldNotSaveUserInvalidEmail() throws Exception {
    request.setEmail("email");
    when(validator.validator(anyString(), anyString())).thenReturn(Boolean.FALSE);
    try {
      UserResponseDto responseDto = userService.register(request);
    } finally {
      verify(validator).validator(any(), any());
    }
  }

  @Test(expected = ValidationException.class)
  public void shouldNotSaveUserInvalidPassword() throws Exception {
    request.setPassword("ee");
    when(validator.validator(anyString(), eq(Constants.EMAIL_REGEX))).thenReturn(Boolean.TRUE);
    try {
      UserResponseDto responseDto = userService.register(request);
    } finally {
      verify(validator, times(2)).validator(any(), any());
    }
  }

  @Test(expected = ValidationException.class)
  public void shouldNotSaveUserEmailAlreadyExists() throws Exception {
    when(validator.validator(anyString(), anyString())).thenReturn(Boolean.TRUE);
    when(usersRepository.findByEmail(anyString())).thenReturn(Optional.of(userEntity));
    try {
      UserResponseDto responseDto = userService.register(request);
    } finally {
      verify(validator, times(2)).validator(any(), any());
      verify(usersRepository).findByEmail(any());
    }
  }

  @Test(expected = Exception.class)
  public void shouldNotSaveUserNoDBConnection() throws Exception {
    when(validator.validator(anyString(), anyString())).thenReturn(Boolean.TRUE);
    when(usersRepository.findByEmail(anyString())).thenReturn(Optional.empty());
    when(usersRepository.save(any())).thenThrow(HttpServerErrorException.class);

    try {
      UserResponseDto responseDto = userService.register(request);
    } finally {
      verify(validator, times(2)).validator(any(), any());
      verify(usersRepository).findByEmail(any());
      verify(usersRepository).save(any());
    }
  }


  @After
  public void tearDown() throws Exception {
    verifyNoMoreInteractions(validator, usersRepository);
  }
}
