package com.users.register.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.users.register.dto.UserResponseDto;
import com.users.register.service.UserService;
import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@WebMvcTest(value = UsersController.class)
@ExtendWith(SpringExtension.class)
class UsersControllerTest {

  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private UserService userService;

  private String request;
  private String response;
  private UserResponseDto userResponse;

  @BeforeEach
  void setUp() {
    request = "{\n"
        + "\t\"name\": \"Loki\",\n"
        + "\t\"email\": \"loki1@gmail.com\",\n"
        + "\t\"password\": \"Hola22\",\n"
        + "\t\"phone\":{\n"
        + "\t\t\"number\": 22334455,\n"
        + "\t\t\"cityCode\": 2,\n"
        + "\t\t\"countryCode\":56\n"
        + "\t}\n"
        + "}";

    response = "{\n"
        + "    \"id\": 1,\n"
        + "    \"created\": \"2020-01-30\",\n"
        + "    \"modified\": \"2020-01-30\",\n"
        + "    \"lastLogin\": \"2020-01-30\",\n"
        + "    \"token\": \"4d236be3-c2db-4b72-9568-285797d7e356\",\n"
        + "    \"active\": true\n"
        + "}";

    userResponse = new UserResponseDto();
    userResponse.setId(1L);
    userResponse.setToken("4d236be3-c2db-4b72-9568-285797d7e356");
    userResponse.setCreated(LocalDate.of(2020,01,30));
    userResponse.setModified(LocalDate.of(2020,01,30));
    userResponse.setLastLogin(LocalDate.of(2020,01,30));
    userResponse.setActive(Boolean.TRUE);
  }

  @Test
  void shouldCreateUser() throws Exception {
    when(userService.register(any())).thenReturn(userResponse);
    mockMvc.perform(post("/register/user")
        .contentType(MediaType.APPLICATION_JSON)
        .content(request))
        .andExpect(status().isCreated())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().json(response));
  }
}
