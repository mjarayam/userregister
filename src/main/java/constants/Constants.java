package constants;

public class Constants {

  /**
   * The Constant regex for email validation
   */
  public static final String EMAIL_REGEX = "^(.+)@(.+)$";
  /**
   * The Constant regex for password validation
   */
  public static final String PASSWORD_REGEX = "^([A-Z]{1})([a-z]+)([0-9]{2})$";

}
