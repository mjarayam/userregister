package com.users.register.controller;

import com.users.register.dto.UserRequestDto;
import com.users.register.dto.UserResponseDto;
import com.users.register.service.UserService;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("register/user")
public class UsersController {

  private UserService userService;

  public UsersController(UserService userService) {
    this.userService = userService;
  }

  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(code = HttpStatus.CREATED)
  public UserResponseDto userRegister(@RequestBody @Valid UserRequestDto userRequestDto)
      throws Exception {
    return userService.register(userRequestDto);
  }
}
