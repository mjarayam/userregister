package com.users.register.dto;

import java.time.LocalDate;
import lombok.Data;

@Data
public class UserResponseDto {

  private Long id;
  private LocalDate created;
  private LocalDate modified;
  private LocalDate lastLogin;
  private String token;
  private boolean active;

  public UserResponseDto() {
  }

  public UserResponseDto(Long id, LocalDate created, LocalDate modified,
      LocalDate lastLogin, String token, boolean active) {
    this.id = id;
    this.created = created;
    this.modified = modified;
    this.lastLogin = lastLogin;
    this.token = token;
    this.active = active;
  }
}
