package com.users.register.dto;

import lombok.Data;

@Data
public class UserPhoneDto {

  private Long number;
  private Long cityCode;
  private Long countryCode;

  public UserPhoneDto() {
  }

  public UserPhoneDto(Long number, Long cityCode, Long countryCode) {
    this.number = number;
    this.cityCode = cityCode;
    this.countryCode = countryCode;
  }
}
