package com.users.register.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UserRequestDto {

  @NotNull
  @NotEmpty
  private String name;
  @NotNull
  @NotEmpty
  private String email;
  @NotNull
  @NotEmpty
  private String password;
  private UserPhoneDto phone;

  public UserRequestDto() {
  }

  public UserRequestDto(@NotNull String name,
      @NotNull String email, @NotNull String password, UserPhoneDto phone) {
    this.name = name;
    this.email = email;
    this.password = password;
    this.phone = phone;
  }
}
