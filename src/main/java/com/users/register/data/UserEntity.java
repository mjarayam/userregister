package com.users.register.data;

import java.time.LocalDate;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "USER")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usr_seq")
  @SequenceGenerator(name = "usr_seq", sequenceName = "usr_seq", allocationSize = 1)
  @Column(name = "usr_id", nullable = false)
  private Long id;

  @Column(name = "usr_name", nullable = false)
  private String name;

  @Column(name = "usr_email", nullable = true)
  private String email;

  @Column(name = "usr_password", nullable = false)
  private String password;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "phone_id", referencedColumnName = "phone_id")
  private UserPhoneEntity phone;

  @Column(name = "usr_token", nullable = false)
  private String token;

  @Column(name = "usr_date_creation", nullable = false)
  private LocalDate creationDate;

  @Column(name = "usr_date_modification", nullable = false)
  private LocalDate lastModification;

  @Column(name = "usr_active", nullable = false)
  private Boolean active;

}
