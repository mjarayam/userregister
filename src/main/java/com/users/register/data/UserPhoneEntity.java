package com.users.register.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "USER_PHONE")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserPhoneEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "phone_seq")
  @SequenceGenerator(name = "phone_seq", sequenceName = "phone_seq", allocationSize = 1)
  @Column(name = "phone_id", nullable = false)
  private Long id;

  @Column(name = "phone_numb", nullable = true)
  private Long number;

  @Column(name = "phone_cityCode", nullable = true)
  private Long cityCode;

  @Column(name = "phone_countryCode", nullable = true)
  private Long countryCode;

  @OneToOne(mappedBy = "phone")
  private UserEntity user;

}
