package com.users.register.service;

import com.users.register.data.UserEntity;
import com.users.register.data.UserPhoneEntity;
import com.users.register.dto.UserRequestDto;
import com.users.register.dto.UserResponseDto;
import com.users.register.repository.UsersRepository;
import com.users.register.service.validator.Validator;
import constants.Constants;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;
import javax.xml.bind.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

  private UsersRepository usersRepository;
  private Validator validator;


  public UserServiceImpl(UsersRepository usersRepository, Validator validator) {
    this.usersRepository = usersRepository;
    this.validator = validator;
  }

  @Override
  public UserResponseDto register(UserRequestDto userRequestDto) throws Exception {
    String email = userRequestDto.getEmail();
    String password = userRequestDto.getPassword();

    if (!validator.validator(email, Constants.EMAIL_REGEX)) {
      log.error("The email is not valid!");
      throw new ValidationException("invalid email");
    } else if (!validator.validator(password, Constants.PASSWORD_REGEX)) {
      log.error("The password is not valid!");
      throw new ValidationException("invalid password");
    }

    Optional<UserEntity> optionalUserEntity = usersRepository
        .findByEmail(email);
    if (optionalUserEntity.isPresent()) {
      log.error("The email already exists in the data base");
      throw new ValidationException("This email is currently in use");
    } else {
      UserPhoneEntity userPhone = new UserPhoneEntity();
      userPhone.setNumber(userRequestDto.getPhone().getNumber());
      userPhone.setCityCode(userRequestDto.getPhone().getCityCode());
      userPhone.setCountryCode(userRequestDto.getPhone().getCountryCode());

      UserEntity user = new UserEntity();
      user.setName(userRequestDto.getName());
      user.setEmail(email);
      user.setCreationDate(LocalDate.now());
      user.setPassword(password);
      user.setToken(getToken());
      user.setLastModification(LocalDate.now());
      user.setPhone(userPhone);
      user.setActive(Boolean.TRUE);

      try {
        UserEntity persistedUser = usersRepository.save(user);

        UserResponseDto userResponse = new UserResponseDto();
        userResponse.setId(persistedUser.getId());
        userResponse.setToken(persistedUser.getToken());
        userResponse.setActive(persistedUser.getActive());
        userResponse.setCreated(persistedUser.getCreationDate());
        userResponse.setLastLogin(persistedUser.getLastModification());
        userResponse.setModified(persistedUser.getLastModification());
        return userResponse;
      } catch (Exception e) {
        log.error("There was a problem connecting to the data base");
        throw new Exception("message: ", e);
      }
    }
  }

  private String getToken() {
    UUID token = UUID.randomUUID();
    return token.toString();
  }
}
