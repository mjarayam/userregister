package com.users.register.service;

import com.users.register.dto.UserRequestDto;
import com.users.register.dto.UserResponseDto;

public interface UserService {

  UserResponseDto register(UserRequestDto userRequestDto) throws Exception;
}
