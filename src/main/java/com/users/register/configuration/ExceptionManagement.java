package com.users.register.configuration;

import com.users.register.dto.ErrorDto;
import javax.xml.bind.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ExceptionManagement {

  @ExceptionHandler({MethodArgumentNotValidException.class, ValidationException.class})
  public final ResponseEntity<ErrorDto> handleException(Exception ex) {
    ErrorDto errorDto = new ErrorDto();
    errorDto.setMessage(ex.getMessage());
    log.error("ERROR: ", ex);
    return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
  }


}
