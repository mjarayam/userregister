# USER REGISTRY MICROSERVICE

# 1. Owner:
María José Araya M.
Contact: mariajose.15@gmail.com

# 2. Postman request to create a user
Request: 

 POST /register/user HTTP/1.1
 Host: localhost:8081
 Content-Type: application/json
``` 
{
    "name": "Loki",
    "email": "loki5@gmail.com",
    "password": "Hola22",
    "phone":{
        "number": 22334455,
        "cityCode": 2,
        "countryCode":56
    }
}
```
 
# 3. DB Scripts:
 
 create sequence usr_seq start 1 increment 1;
 create sequence phone_seq start 1 increment 1;
 
``` 
create table USER_PHONE
(
    PHONE_ID          BIGINT not null,
    PHONE_NUMB        BIGINT,
    PHONE_CITYCODE    BIGINT,
    PHONE_COUNTRYCODE BIGINT,
    constraint PHONE_ID_PKEY
        primary key (PHONE_ID)
);
```
```
create table USER
(
    USR_ID                BIGINT       not null,
    USR_NAME              VARCHAR(255) not null,
    USR_EMAIL             VARCHAR(255),
    USR_PASSWORD          VARCHAR(255) not null,
    USR_TOKEN             VARCHAR(255) not null,
    USR_DATE_CREATION     DATE         not null,
    USR_DATE_MODIFICATION DATE         not null,
    USR_ACTIVE            BOOLEAN      not null,
    PHONE_ID              BIGINT,
    constraint USR_ID_PKEY
        primary key (USR_ID),
    constraint PHONE_ID
        foreign key (PHONE_ID) references USER_PHONE (PHONE_ID)
);
```
